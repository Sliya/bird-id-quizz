# Bird ID Quizz

Bird ID Quizz is a game to improve my bird id skills. A picture of a bird appears on the screen and I have to identify the species.

## Installation

```bash
npm install
```

## Usage

```python
FLICKR_API_KEY=<KEY> npm run dev
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
